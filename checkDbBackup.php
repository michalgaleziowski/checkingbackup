<?php
/* 																																 */
/*	Script connects with database host to get all databases names, then checking if database backup has been created 			 */
/*  in /mnt/nsa310/backup/mysql_backup/backup_all_db_separate/ folder.										 														 */
/*	Author: Michal Galeziowski 																									 */
/*																																 */

	$headers = 'From: ***'.PHP_EOL.'Reply-To:'.PHP_EOL.'Content-type: text/html; charset=iso-8859-2';
	$email_to =  '***';
	$without_backup = array();
		try{
			// Database connection
			$conn = new PDO('mysql:host=***; dbname=;port=13306', '***', '***');
			$stmt = $conn->prepare('SHOW DATABASES where `database` not in (\'information_schema\',\'performance_schema\');');
			$stmt->execute();
			$res  = $stmt->fetchAll();
			$dbs = '';
			$backup_path = '/mnt/nsa310/backup/mysql_backup/backup_all_db_separate/'.date("ymd").'/';
			foreach ($res as $value) {
				if(!file_exists($backup_path.$value[0].'.sql')){
					$without_backup[] = $value[0];
					$dbs .= '(\''.$value[0].'\',\''.date("Y-m-d H:i:s").'\'),';
					//file_put_contents('checkDbBackupLOG.txt','['.date("d/M/Y H:i:s").'] '.'Cannot find backup: '.$value[0].PHP_EOL, FILE_APPEND);	
				}else{
					if(filesize($backup_path.$value[0].'.sql') <= 0){
						$without_backu[] = $value[0];
						$dbs .= '(\''.$value[0].'\',\''.date("Y-m-d H:i:s").'\'),';
					}
				}

			}
			$val = substr($dbs, 0, -1);
				
			$stmt = $conn->prepare('INSERT INTO michalg.check_backup_db(schema_without_backup, date) VALUES '.$val.';');
			$stmt->execute();
			
			
		}catch(PDOexception $e){
			file_put_contents('checkDbBackupLOG.txt', '['.date("d/M/Y H:i:s").']'.$e.PHP_EOL, FILE_APPEND);	
		}	
?>
