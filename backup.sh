#!/bin/bash
# Pawel Pajchrowski
# backup mysql db into one file
# backup each mysql db into a different file, rather than one big file
# as with --all-databases - will make restores easier

#USER="all_db_backup"
#PASSWORD="123cycki"


#ARRAYS WITH TABLES TO EXCLUDE NEEDS TO BE DECLARED HERE IN FOLLOWING FORMAT: schema_name=('table_to_exclude1' 'table_to_exclude2')
competitors=('active_auction' 'active_auction2' 'auction_history' 'history' 'history_dla_arka' 'history_old' 'history_test' 'history_to_insert' 'history_v2' 'luxgrill' 'min_dates' 'orders_exceptions_back')
ebay_api_calls=('access_page' 'active_auction' 'active_auction_20130903' 'active_auction_20140807' 'active_auction_20140919' 'active_auction_20141006' 'active_auction_all' 'active_auction_all_log' 'active_auction_bad' 'active_auction_description' 'active_auction_from_20140901' 'active_auction_from_20141001' 'active_auction_oils' 'active_auction_shipping_excluded' 'active_auction_variation' 'active_auction_variation_valuelist' 'auction_auto_qty_20140919' 'auction_history' 'auction_history_log' 'calls' 'ebay_calls_log' 'ebay_promotion_items' 'ebay_promotions' 'grupy' 'not_gtc_20110820' 'inactive_auction' 'paczki' 'parametry' 'query' 'trackingi_dane' 'turbodziobak_job_history' 'wywalone_podbijacz')
#
#

USER="root"
PASSWORD="beta2010"
OUTPUTFILEALL="/mnt/nsa310/backup/mysql_backup/backup_all_db/backup_$(date +%y%m%d).sql.gz"
OUTPUTDIRSEP="/home/pawelp/backup_db/$(date +%y%m%d)"
OUTPUTDIRSEPNSA="/mnt/nsa310/backup/mysql_backup/backup_all_db_separate/$(date +%y%m%d)"

MYSQLDUMP="/usr/bin/mysqldump"
MYSQL="/usr/bin/mysql"



#echo "Backup all BD - start"
#$MYSQLDUMP --opt --single-transaction -u $USER -p$PASSWORD --all-databases --events|gzip > $OUTPUTFILEALL
#echo "Backup all DB - finish"

echo "Create folder"
# create folder for backup files
mkdir $OUTPUTDIRSEP

# clean up any old backups - save space
rm "$OUTPUTDIRSEP/*sql" > /dev/null 2>&1

echo "Backup all DB (separated files) - start"

# get a list of databases
databases=`$MYSQL --user=$USER --password=$PASSWORD -e "SHOW DATABASES where \\\`database\\\` not in ('information_schema','performance_schema');" | tr -d "| " | grep -v Database`

sh /var/www/backup_db/repair_connection.sh

#Sprawdza czy folder zapisu istnieje
if [ ! -d "/mnt/nsa310" ]; then
    echo "Braka polaczenia z serwerem nas"
else

# create folder for backup files
mkdir $OUTPUTDIRSEPNSA


# dump each database in turn
for db in ${databases[@]}; do

	# Creating --ignore-table string for each schema, which is mentioned
	tabtoskip=''
	eval array=\( \${$db[@]} \)
	for schem in ${array[@]}; do
		tabtoskip=${tabtoskip}' '${ign}${db}'.'${schem}
	done
    
    # Calling for mysqldump 
    $MYSQLDUMP --opt --single-transaction -u $USER -p$PASSWORD --databases --events $db $tabtoskip > "$OUTPUTDIRSEP/$db.sql"

    #Kopiowanie na dysk sieciowy

    cp -r "$OUTPUTDIRSEP/$db.sql" "$OUTPUTDIRSEPNSA/$db.sql"
    #bar -of "$OUTPUTDIRSEPNSA/$db.sql" --in-file "$OUTPUTDIRSEP/$db.sql"

    #usuwanie z serwera
    rm -rf "$OUTPUTDIRSEP/$db.sql"

done

# clean up any old backups - save space
rm -rf "$OUTPUTDIRSEP"

umount /mnt/nsa310

echo "Backup all DB (separated files) - finish"
echo "Backup DONE"

fi
